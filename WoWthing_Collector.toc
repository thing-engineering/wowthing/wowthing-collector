## Interface: 90005
## Title: WoWthing Collector
## Notes: Collects data to sync with WoWthing.
## Version: 9.0.5.4
## Author: Freddie
## SavedVariables: WWTCSaved

libs\LibStub\LibStub.lua
libs\LibRealmInfo17janekjl\LibRealmInfo17janekjl.lua

Collector.lua
